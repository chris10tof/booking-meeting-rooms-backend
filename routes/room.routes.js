const router = require('express').Router();

const roomController = require('../controllers/room.controller')

router.get('/getAllRooms', roomController.getAllRooms);
router.get('/getAllRoomCapacities', roomController.getAllRoomCapacities);
router.get('/getAllEquipmentType', roomController.getAllEquipmentType);

module.exports = router;