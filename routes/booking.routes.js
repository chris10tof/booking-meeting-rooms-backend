const router = require('express').Router();
const bookingController = require('../controllers/booking.controller')

router.get('/getAllBookings', bookingController.getAllBookings);

module.exports = router;