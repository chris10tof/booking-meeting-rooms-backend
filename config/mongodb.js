///  DOCS pour la connection au cluster de MongoDB avec Atlas et MongoDB CMompass : 
// https://docs.atlas.mongodb.com/best-practices-connecting-from-aws-lambda/#std-label-lambda-aws-example

const mongoose = require('mongoose');

const URI = `${process.env.MONGODB_URI_START}${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}${process.env.MONGODB_URI_END}${process.env.MONGODB_DATABASE_NAME}${process.env.MONGODB_URI_PARAMS}`

console.log( "URI CONNEXION URI ====>   \n", URI);

mongoose
    .connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log("Connected to MongoDB DONE !"))
    .catch(error => console.log(error));
