const ObjectID = require('mongoose').Types.ObjectId;
const BookingModel = require('../models/booking.model');

var hourStart = new Date();
var hourEnd = new Date();

// 
module.exports.getAllBookings = async(req, res) => {
    const bookings = await BookingModel.find().select();
    res.status(200).json(bookings);
}
