const Logger = require("nodemon/lib/utils/log");
const RoomModel = require("../models/room.model");

// Renvoie toutes les informations des salles de réunion d'ADLIN
module.exports.getAllRooms = async(req, res) => {
    const AdlinAllRooms = await RoomModel.find().select();
    console.log(AdlinAllRooms.rooms);
    res.status(200).json(AdlinAllRooms);
};

// Renvoie toutes les possibilités de capacité des salles existantes
module.exports.getAllRoomCapacities = async(req, res) => {

};

// Renvoie tous les types d'équipements possibles
module.exports.getAllEquipmentType = async(req, res) => {

};