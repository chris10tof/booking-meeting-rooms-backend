const mongoose = require('mongoose')

const roomSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        capacity: {
            type: Number,
            required: true
        },
        equipements: {
            type: Array,
            required: true
        },
        createdAt: {
            type: Date,
        },
        updatedAt: {
            type: Date,
        },
    }, 
    {
        timestamps: true
    }
)

const RoomModel = mongoose.model('room', roomSchema)

module.exports = RoomModel