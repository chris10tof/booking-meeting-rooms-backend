const mongoose = require('mongoose')

const bookingSchema = new mongoose.Schema({
    roomGuid: {
        type: String,
        required: true
    },
    isBooked: {
        type: Boolean,
        required: true
    },
    bookingTimeStart: {
        type: Date,
        required: true
    },
    bookingTimeEnd: {
        type: Date,
        required: true
    }
}, {timestamps: true})

const BookingModel = mongoose.model('booking', bookingSchema)

module.exports = BookingModel