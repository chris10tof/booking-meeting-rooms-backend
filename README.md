# Adlin Booking Meeting Rooms Partie BACKEND


- To run the app : [install.md](docs/install.md)
- Documentation of my API : [API.md](docs/API.md)

***Note:*** Work in Progress

## Stack technique

Here is the list of technologies used by the application :

| Technologie Name | GitHub Link | NPM Link |
|--|--|--|
|cors 2.8.5| https://github.com/expressjs/cors | https://www.npmjs.com/package/cors |
|dotenv 16.0.0| https://github.com/motdotla/dotenv | https://www.npmjs.com/package/dotenv |
|[express 4.17.3](http://expressjs.com/)| https://github.com/expressjs/express | https://www.npmjs.com/package/express |
|[mongoose 6.2.4](https://mongoosejs.com/)| https://github.com/Automattic/mongoose | https://www.npmjs.com/package/mongoose |
|morgan 1.10.0| https://github.com/expressjs/morgan | https://www.npmjs.com/package/morgan |
|[nodemon 2.0.15](https://nodemon.io/)| https://github.com/remy/nodemon | https://www.npmjs.com/package/nodemon |

## INFO HELP

a server hosted in the cloud with MongoDB Atlas was used

Do not hesitate to use [POSTMAN](https://www.postman.com/) to test requests locally
