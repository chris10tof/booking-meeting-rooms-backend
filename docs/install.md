# How to run application

- First clone the repository of course : 
`git clone https://example.com`
- Change for the directory source : 
`cd ../path/to/the/file`
- `npm install`
- Complete your .env with your database log
- `npm start` to run the server
