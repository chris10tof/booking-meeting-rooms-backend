# List of the custom routes

List of the routes of project :

| Methods| Routes                           | Utility  |  |
|--------|----------------------------------|--------------------------|--|
|GET     | /api/booking/getAllBookings      | Returns all reservations |  `DONE` |
|GET     | /api/room/getAllRooms            | Returns all meeting room information from ADLIN | `DONE` |
|GET     | /api/room/getAllRoomCapacities   | Returns all capacity possibilities of existing rooms | `WIP` |
|GET     | /api/room/getAllEquipmentType    | Returns all possible equipment types | `WIP` |