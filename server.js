const express = require('express');

const bookingRoutes = require('./routes/booking.routes');
const roomRoutes = require('./routes/room.routes');

require('dotenv').config({path: './config/.env'});
require('./config/mongodb');
const cors = require('cors');

const app = express();

const corsOptions = {
    origin: process.env.CLIENT_URL,
    credentials: true,
    'allowedHeaders': [
        'sessionId', 'Content-Type'
    ],
    'exposedHeaders': ['sessionId'],
    // 'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'methods': 'GET,POST,',
    'preflightContinue': false
}
app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// routes
app.use('/api/booking', bookingRoutes);
app.use('/api/room', roomRoutes);

// server
app.listen(process.env.LISTEN_PORT, () => {
    console.log(`Listening on port ${process.env.LISTEN_PORT}`);
})